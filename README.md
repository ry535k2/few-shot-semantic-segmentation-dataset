## Few-shot Semantic Segmentation Dataset

This repository contains a selection of images from LVIS (from both train and val subsets) that form the dataset 
proposed in the Master's thesis with the title Analysis of Methods for Few-shot Semantic Segmentation
and supervisor prof. dr. Matej Kristan.

Four CSV files represent four folds of the dataset, three are used for training, the last one for testing. Each CSV file contains three columns,
the first one is for the class in LVIS dataset, the second one is for the subclass in our dataset, the third one 
is the name of the image file as appears in LVIS. 